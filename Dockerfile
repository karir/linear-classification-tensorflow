FROM tensorflow/tensorflow:0.9.0
MAINTAINER Sky Kok

RUN pip install pandas

ADD adult.data /notebooks/adult.data
ADD adult.test /notebooks/adult.test
ADD small.csv /notebooks/small.csv
ADD basic_linear_classification.ipynb /notebooks/basic_linear_classification.ipynb
ADD official_tensorflow_linear_classification_tutorial.ipynb /notebooks/official_tensorflow_linear_classification_tutorial.ipynb
